时间 | 开发内容 | 验收标准 | 完成情况 
---|--- |--- | --- |
7.1 ~ 7.9 | 学习braft使用，学习chunkserver | 输出学习文档 
7.10~7.16 | 调研chubaofs，tikv的心跳合并实现 | 输出调研文档
7.17~7.25|设计chunkserver的心跳合并方案 | 输出设计文档，评审通过
7.26~8.10|代码开发，完成基本demo| 开发代码，提交pr
8.10~8.14|测试 | 基本功能走通，输出测试报告	 